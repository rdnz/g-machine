module FInterface
  (module FInterface, Value (Bool, Integer), evaluationStepCount)
  where

import F

import Relude

eval :: Text -> Either String Value
eval = evaluationResult

