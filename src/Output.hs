module Output where

import F.Lex (lex)
import F.Parse (parse)
import F.Compile (compileProgram)
import F.MF (HeapAddress, StackElement (HeapAddress, ReturnAddress))
import F.MF qualified as MF
import F qualified as F

import Data.Text qualified as T
import Data.Text.Lazy.Builder qualified as TB
import Data.Map.Strict qualified as M
import Data.HashMap.Strict qualified as HM
import Relude
import Relude.Extra (maximum1)

compileAndEmulate :: Text -> Text
compileAndEmulate program =
  case (=<<) compileProgram $ (=<<) parse $ lex $ program of
    Left message -> T.pack (labelError message)
    Right mfProgram ->
      "Compilation result\n\n" <>
      renderMFProgram mfProgram <>
      "\n\nEmulation trace\n\n" <>
      emulateHelp mfProgram

evaluationResult :: Text -> Text
evaluationResult =
  T.pack .
  (<> "\n") .
  either labelError show .
  F.evaluationResult

evaluationStepCount :: Text -> Text
evaluationStepCount =
  T.pack .
  (<> "\n") .
  either labelError show .
  F.evaluationStepCount

compile :: Text -> Text
compile =
  either (T.pack . labelError) id .
  fmap renderMFProgram .
  (=<<) compileProgram .
  (=<<) parse .
  lex

emulate :: Text -> Text
emulate =
  either (T.pack . labelError) id .
  fmap emulateHelp .
  (=<<) compileProgram .
  (=<<) parse .
  lex

emulateHelp :: MF.Program -> Text
emulateHelp program
  | (trace, result) <- MF.emulate program =
    T.pack $
      printTrace trace <>
      either (("\n" <>) . (<> "\n")) (const mempty) result

labelError :: String -> String
labelError = ("Error: " <>) . (<> "\n")

renderMFProgram :: MF.Program -> Text
renderMFProgram (MF.Program instructions globalEnvironment) =
  (toStrict . TB.toLazyText)
    (
      "globalEnvironment =\n" <>
      (
        renderList $
        sortOn (\(_ , (_, (MF.MakeProgramCounter b))) -> b) $
        HM.toList $
        globalEnvironment
      ) <>
      "\ninstructionsMf =\n" <>
      renderList instructions
    )

renderList :: (Show element) => [element] -> TB.Builder
renderList list =
      "  [\n" <>
      (
        fold $
        intersperse ",\n" $
        fmap ("    " <>) $
        fmap TB.fromString $
        fmap show $
        list
      ) <>
      "\n  ]\n"

padRight :: Int -> String -> String
padRight width text = text <> replicate (width - length text) ' '

printStackElement :: StackElement -> String
printStackElement (HeapAddress a) = "h" <> show a
printStackElement (ReturnAddress a) = "c" <> show a

printStack :: [StackElement] -> [String]
printStack =
  zipWith printLine [(0 :: Integer)..]
  where
    printLine :: Integer -> StackElement -> String
    printLine i e =
      "s" <> padRight 4 (show i <> ": ") <> printStackElement e

printHeap :: Map HeapAddress MF.Node -> [String]
printHeap heap =
  fmap (uncurry printLine) (M.toList heap)
  where
    printLine :: HeapAddress -> MF.Node -> String
    printLine i e =
      "h" <> padRight 4 (show i <> ": ") <> show e

columns :: Int -> [String] -> [String] -> [String]
columns width0 column0 column1 =
  zipWith
    (<>)
    (fmap (padRight width0) column0PaddedBottom)
    column1PaddedBottom
  where
    -- width0 = maybe 0 maximum1 (nonEmpty $ length <$> column0) + 5
    height = max (length column0) (length column1)
    column0PaddedBottom = column0 <> replicate (height - length column0) ""
    column1PaddedBottom = column1 <> replicate (height - length column1) ""

printState :: Int -> Int -> MF.State -> String
printState width0 width1 state@(MF.State stack heap _ _ _ _) =
  foldMap (<> "\n") (columns width0 (printRegisters state) (columns width1 (printStack stack) (printHeap heap)))

printRegisters :: MF.State -> [String]
printRegisters (MF.State _ _ programCounter instruction _ _) =
  [
    "I: " <> show instruction,
    "P: " <> show programCounter
  ]        

printTrace :: [MF.State] -> String
printTrace states =
  fold $
  intersperse "\n" $
  fmap (printState width0 width1) $
  states
  where
    width0 =
      (maybe 0 maximum1 $ nonEmpty $ fmap length $ foldMap printRegisters $ states)
      +
      5
    width1 =
      (maybe 0 maximum1 $ nonEmpty $ fmap length $ foldMap (printStack . MF.stack) $ states)
      +
      5
