module F (module F, Value (Integer, Bool)) where

import F.Lex (lex)
import F.Parse (parse)
import F.Compile (compileProgram)
import F.MF (emulationResult, emulationStepCount)
import F.Common (Value (Bool, Integer))

import Relude

evaluationResult :: Text -> Either String Value
evaluationResult =
  (=<<) emulationResult . (=<<) compileProgram . (=<<) parse . lex

evaluationStepCount :: Text -> Either String Int
evaluationStepCount =
  fmap emulationStepCount . (=<<) compileProgram . (=<<) parse . lex
