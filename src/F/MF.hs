module F.MF where

import F.Common (Name, Value (Integer, Bool))

import Data.Vector (Vector)
import Data.Vector qualified as V
import Data.Map.Strict qualified as M
import Data.HashMap.Strict qualified as HM
import Relude hiding (State, Type)

data Program =
  Program [Instruction] (HashMap Name (Natural, ProgramCounter))
  deriving (Show)

newtype HeapAddress =
  MakeHeapAddress Int deriving newtype (Show, Eq, Ord)
newtype ProgramCounter =
  MakeProgramCounter Int deriving newtype (Show, Enum)

data Node =
  DEF Name Natural ProgramCounter |
  APP HeapAddress HeapAddress |
  VAL Value |
  IND HeapAddress |
  UNINITIALIZED
  deriving (Show)

data Operator =
  Not |
  Negate |
  Or | And |
  Plus | Minus | Times | DividedBy |
  Equals | LessThan |
  If
  deriving (Show)

data Instruction =
  Reset |
  Pushval Value |
  Pushfun Name |
  Pushparam Integer |
  Makeapp |
  Operator Operator |
  Alloc |
  SlideLet Natural |
  Update Natural |
  Slide Natural |
  Unwind |
  Call |
  Return |
  Halt
  deriving (Show)

data StackElement =
  HeapAddress HeapAddress |
  ReturnAddress ProgramCounter
  deriving (Show)

data State =
  State
    {
      stack :: [StackElement],
      heap :: Map HeapAddress Node,
      programCounter :: ProgramCounter,
      instruction :: Instruction,
      globals :: HashMap Name HeapAddress,
      code :: Vector Instruction
    }
    deriving (Show)

setStack :: [StackElement] -> State -> State
setStack stackNew stateOld = stateOld {stack = stackNew}

setHeap :: Map HeapAddress Node -> State -> State
setHeap heapNew stateOld = stateOld {heap = heapNew}

setInstruction :: Instruction -> State -> State
setInstruction instructionNew stateOld =
  stateOld {instruction = instructionNew}

setProgramCounter :: ProgramCounter -> State -> State
setProgramCounter programCounterNew stateOld =
  stateOld {programCounter = programCounterNew}

-- precondition: next instruction in `code ! programCounter`
-- postcondition: next instruction in `code ! programCounter`
step :: State -> Either (Either String Value) State
step stateOld@(State _ _ programCounterOld _ _ code)
  | Just instruction <- code V.!? coerce programCounterOld =
    stepWithoutProgramCounter $
    setProgramCounter (succ programCounterOld) $
    setInstruction instruction $
    stateOld
  | otherwise =
    Left $
    Left $
    "cannot step state with invalid program counter " <> show stateOld

-- precondition:
--   next instruction in `instruction`,
--   instruction after next instruction in `code ! programCounter`
-- postcondition: next instruction in `code ! programCounter`
stepWithoutProgramCounter :: State -> Either (Either String Value) State
stepWithoutProgramCounter
  stateOld@(State stackOld heapOld programCounterOld instruction globals _)
  | Reset <- instruction = Right stateOld
  |
    Halt <- instruction,
    [HeapAddress address] <- stackOld,
    Just (VAL result) <- M.lookup address heapOld
    = Left $ Right $ result
  | Pushfun name <- instruction,
    Just address <- HM.lookup name globals
    =
      Right $
      setStack (HeapAddress address : stackOld) $
      stateOld
  | Pushval value <- instruction = Right (pushValue value stackOld)
  |
    Pushparam i <- instruction,
    Just i <- integerToNatural (i + 1),
    Just (_, HeapAddress address : _) <- splitAtExactly i stackOld,
    Just (APP _ argument) <- M.lookup address heapOld
    =
      Right $
      setStack (HeapAddress argument : stackOld) $
      stateOld
  |
    Makeapp <- instruction,
    HeapAddress address1 : HeapAddress address0 : stackRest <- stackOld
    =
      let
        (heapNew, addressNew) =
          heapInsert (APP address1 address0) heapOld
      in
        Right $
        setStack (HeapAddress addressNew : stackRest) $
        setHeap heapNew $
        stateOld
  |
    Slide n <- instruction,
    result@(HeapAddress _) : returnAddress@(ReturnAddress _) : stackOld <-
      stackOld,
    Just stackRest <- dropExactly n stackOld
    =
      Right $
      setStack (result : returnAddress : stackRest) $
      stateOld
  |
    SlideLet n <- instruction,
    result@(HeapAddress _) : stackOld <- stackOld,
    Just stackRest <- dropExactly n stackOld
    =
      Right $
      setStack (result : stackRest) $
      stateOld
  |
    Unwind <- instruction,
    HeapAddress address : _ <- stackOld,
    Just (VAL _) <- M.lookup address heapOld
    = Right stateOld
  |
    Unwind <- instruction,
    HeapAddress address : _ <- stackOld,
    Just (APP address1 _) <- M.lookup address heapOld
    =
      Right $
      setProgramCounter (pred programCounterOld) $
      setStack (HeapAddress address1 : stackOld) $
      stateOld
  |
    Unwind <- instruction,
    HeapAddress address : _ <- stackOld,
    Just (DEF _name _arity _address) <- M.lookup address heapOld
    = Right stateOld
  |
    Unwind <- instruction,
    HeapAddress address0 : stackRest <- stackOld,
    Just (IND address1) <- M.lookup address0 heapOld
    =
      Right $
      setProgramCounter (pred programCounterOld) $
      setStack (HeapAddress address1 : stackRest) $
      stateOld
  |
    Call <- instruction,
    HeapAddress address : _ <- stackOld,
    Just (VAL _) <- M.lookup address heapOld
    = Right stateOld
  |
    Call <- instruction,
    HeapAddress address0 : _ <- stackOld,
    Just (DEF _name _arity address1) <- M.lookup address0 heapOld
    =
      Right $
      setProgramCounter address1 $
      setStack (ReturnAddress programCounterOld : stackOld) $
      stateOld
  |
    Operator Not <- instruction,
    HeapAddress address : stackRest <- stackOld,
    Just (VAL (Bool operand)) <- M.lookup address heapOld
    = Right $ pushValue (Bool $ not $ operand) stackRest
  |
    Operator Negate <- instruction,
    HeapAddress address : stackRest <- stackOld,
    Just (VAL (Integer operand)) <- M.lookup address heapOld
    = Right $ pushValue (Integer $ negate $ operand) stackRest
  |
    Operator If <- instruction,
    address2@(HeapAddress _) : address1@(HeapAddress _) : HeapAddress address0 : stackRest <- stackOld,
    Just (VAL (Bool operand0)) <- M.lookup address0 heapOld
    =
      Right $
      setStack ((if operand0 then address1 else address2) : stackRest) $
      stateOld
  |
    Operator operator <- instruction,
    HeapAddress address1 : HeapAddress address0 : stackRest <- stackOld,
    Just (VAL operand0) <- M.lookup address0 heapOld,
    Just (VAL operand1) <- M.lookup address1 heapOld,
    Just result <- binaryPrimitive operator operand0 operand1
    = Right (pushValue result stackRest)
  |
    Return <- instruction,
    result : ReturnAddress returnAddress : stackRest <- stackOld
    =
      Right $
      setProgramCounter returnAddress $
      setStack (result : stackRest) $
      stateOld
  |
    Update n <- instruction,
    Just (HeapAddress result : _, HeapAddress root : _) <-
      splitAtExactly (n + 2) stackOld,
    Just heapNew <- heapSet root (IND result) heapOld
    =
      Right $
      setHeap heapNew $
      stateOld
  | Alloc <- instruction = Right (alloc1 stateOld)
  | otherwise = Left $ Left $ "cannot step invalid state " <> show stateOld
  where
    pushValue :: Value -> [StackElement] -> State
    pushValue value stackRest =
      let
        (heapNew, addressNew) = heapInsert (VAL value) heapOld
      in
        setStack (HeapAddress addressNew : stackRest) $
        setHeap heapNew  $
        stateOld
    binaryPrimitive :: Operator -> Value -> Value -> Maybe Value
    binaryPrimitive operator (Bool operand0) (Bool operand1) =
      case operator of
        Or -> Just $ Bool $ operand0 || operand1
        And -> Just $ Bool $ operand0 && operand1
        Equals -> Just $ Bool $ operand0 == operand1
        _ -> Nothing
    binaryPrimitive DividedBy _ (Integer 0) = Nothing
    binaryPrimitive operator (Integer operand0) (Integer operand1) =
      case operator of
        Plus -> Just $ Integer $ operand0 + operand1
        Minus -> Just $ Integer $ operand0 - operand1
        Times -> Just $ Integer $ operand0 * operand1
        DividedBy -> Just $ Integer $ operand0 `div` operand1
        Equals -> Just $ Bool $ operand0 == operand1
        LessThan -> Just $ Bool $ operand0 < operand1
        _ -> Nothing
    binaryPrimitive _ _ _ = Nothing

alloc1 :: State -> State
alloc1 stateOld@(State {stack = stackOld, heap = heapOld}) =
  let (heapNew, addressNew) = heapInsert UNINITIALIZED heapOld
  in
    setStack (HeapAddress addressNew : stackOld) $
    setHeap heapNew $
    stateOld

dropExactly :: Natural -> [a] -> Maybe [a]
dropExactly n list = snd <$> splitAtExactly n list

splitAtExactly :: Natural -> [a] -> Maybe ([a], [a])
splitAtExactly 0 list = Just ([], list)
splitAtExactly i (x:xs)
  | i < 0 = Nothing
  | otherwise =
    case splitAtExactly (pred i) xs of
      Nothing -> Nothing
      Just (xs', xs'') -> Just (x:xs', xs'')
splitAtExactly _ _ = Nothing

heapSet ::
  HeapAddress ->
  Node ->
  Map HeapAddress Node ->
  Maybe (Map HeapAddress Node)
heapSet address node heap
  | M.member address heap = Just (M.insert address node heap)
  | otherwise = Nothing

heapInsert ::
  Node -> Map HeapAddress Node -> (Map HeapAddress Node, HeapAddress)
heapInsert node heap
  | M.member address heap =
    error "firstFree should not return any key the map contains already"
  | otherwise = (M.insert address node heap, address)
  where
    address :: HeapAddress
    address = (coerce firstFree) (M.keys heap)

firstFree :: [Int] -> Int
firstFree list =
  maybe (length list) fst $
  find (uncurry (!=)) $
  zip [0..] list

(!=) :: (Eq a) => a -> a -> Bool
(!=) = (/=)

myUnfoldr :: (a -> Either b a) -> a -> ([a], b)
myUnfoldr f =
  go
  where
    go a
      | Right a <- f a = first (a :) (go a)
      | Left b <- f a = ([], b)

emulate :: Program -> ([State], Either String Value)
emulate (Program code globalEnvironmentCode) =
  myUnfoldr step stateInitial
  where
    stateInitial :: State
    stateInitial =
      State
        []
        heapInitial
        (MakeProgramCounter 0)
        (error "`instruction` is no source of truth")
        globalEnvironmentHeapInitial
        (V.fromList code)
    (heapInitial, globalEnvironmentHeapInitial) =
      HM.foldlWithKey'
        (fromRight (error " no duplicate keys in maps") .:. buildHeap)
        (M.empty, HM.empty)
        globalEnvironmentCode
    buildHeap ::
      (Map HeapAddress Node, HashMap Name HeapAddress) ->
      Name -> (Natural, ProgramCounter) ->
      Either String (Map HeapAddress Node, HashMap Name HeapAddress)
    buildHeap (heapInterim, environmentInterim) name (arity, programCounter) =
      let
        (heapNew, address) =
          heapInsert (DEF name arity programCounter) heapInterim
      in
        (heapNew,) <$> globalsInsert name address environmentInterim

globalsInsert ::
  (Eq key, Show key, Hashable key) =>
  key -> value -> HashMap key value -> Either String (HashMap key value)
globalsInsert key value hashMap
  | HM.member key hashMap = Left ("multiple declarations of " <> show key)
  | otherwise = Right (HM.insert key value hashMap)

emulationResult :: Program -> Either String Value
emulationResult =
  snd . emulate

emulationStepCount :: Program -> Int
emulationStepCount = length . fst . emulate

(.:.) :: (d -> e) -> (a -> b -> c -> d) -> a -> b -> c -> e
(.:.) = (.) . (.) . (.)
infixr 8 .:.
