module F.F where

import F.Common (Name)

import Relude

data Expression =
  Variable Name |
  Natural Natural |
  Application Expression Expression |
  Let (NonEmpty (Name, Expression)) Expression
  deriving (Show)

data TopLevelDefinition =
  TopLevelDefinition
    {
      name :: Name,
      parameters :: [Name],
      body :: Expression
    }
  deriving (Show)

type Program = NonEmpty TopLevelDefinition
