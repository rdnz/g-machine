module F.Common where

import Relude

newtype Name = MakeName Text deriving newtype (Show, Eq, Hashable)

data Value = Integer Integer | Bool Bool deriving (Show, Eq)
