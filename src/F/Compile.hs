module F.Compile where

import F.Common (Name (MakeName), Value (Integer, Bool))
import F.F qualified as F
import F.MF
  (
    Instruction
      (
        Reset,
        Pushval,
        Pushfun,
        Pushparam,
        Makeapp,
        Operator,
        Alloc,
        SlideLet,
        Update,
        Slide,
        Unwind,
        Call,
        Return,
        Halt
      ),
      globalsInsert,
  )
import F.MF qualified as MF

import Data.HashMap.Strict qualified as HM
import Relude

compileProgram ::
  F.Program -> Either String MF.Program
compileProgram program =
  fmap
    (MF.Program
      (programBegin <> foldMap (\(_,_,x) -> x) compiledTopLevelDefinitions)
    ) $
  fmap fst $
  foldlM -- to-do. `scan`?
    buildGlobalEnvironmentCode
    (HM.empty, MF.MakeProgramCounter $ length programBegin)
    compiledTopLevelDefinitions
  where
    programBegin :: [MF.Instruction]
    programBegin = [Reset, Pushfun (MakeName "main"), Call, Halt]
    compiledTopLevelDefinitions ::
      NonEmpty (Name, Natural, [MF.Instruction])
    compiledTopLevelDefinitions =
      fmap compileTopLevelDefinition program <> compiledPrimitives
    buildGlobalEnvironmentCode ::
      (HashMap Name (Natural, MF.ProgramCounter), MF.ProgramCounter) -> -- to-do. use strict data type or strict smart constructor
      (Name, Natural, [MF.Instruction]) ->
      Either
        String
        (HashMap Name (Natural, MF.ProgramCounter), MF.ProgramCounter)
    buildGlobalEnvironmentCode
      (environmentInterim, currentProgramCounter)
      (name, arity, compiledBody)
      =
        (
          ,
          MF.MakeProgramCounter -- to-do. maybe do not do this but keep recomputing the total length
            (coerce currentProgramCounter + length compiledBody)
        )
        <$>
        globalsInsert
          name
          (arity, currentProgramCounter)
          environmentInterim

compiledPrimitives :: NonEmpty (Name, Natural, [MF.Instruction])
compiledPrimitives =
  (MakeName "false", 0, [Pushval (Bool False), Update 0, Slide 1, Unwind, Call, Return]) :|
  (MakeName "true", 0, [Pushval (Bool True), Update 0, Slide 1, Unwind, Call, Return]) :
  (MakeName "not", 1, [Pushparam 1, Unwind, Call, Operator MF.Not, Update 1, Slide 2, Return]) :
  (MakeName "negate", 1, [Pushparam 1, Unwind, Call, Operator MF.Negate, Update 1, Slide 2, Return]) :
  binaryPrimitive "|" MF.Or :
  binaryPrimitive "&" MF.And :
  binaryPrimitive "+" MF.Plus :
  binaryPrimitive "-" MF.Minus :
  binaryPrimitive "*" MF.Times :
  binaryPrimitive "/" MF.DividedBy :
  binaryPrimitive "==" MF.Equals :
  binaryPrimitive "<" MF.LessThan :
  (MakeName "if", 3, [Pushparam 1, Unwind, Call, Pushparam 3, Pushparam 5, Operator MF.If, Update 3, Slide 4, Unwind, Call, Return]) :
  []
  where
    binaryPrimitive ::
      Text -> MF.Operator -> (Name, Natural, [MF.Instruction])
    binaryPrimitive name operator =
      (
        MakeName name,
        2,
        [
          Pushparam 1, Unwind, Call,
          Pushparam 3, Unwind, Call,
          Operator operator,
          Update 2, Slide 3, Return
        ]
      )

compileTopLevelDefinition ::
  F.TopLevelDefinition -> (Name, Natural, [MF.Instruction])
compileTopLevelDefinition (F.TopLevelDefinition name parameters body) =
  (
    name
    ,
    parametersCount
    ,
    compileExpression body (HM.fromList $ zip parameters [1..]) <>
    [
      Update parametersCount,
      Slide (parametersCount + 1),
      Unwind,
      Call,
      Return
    ]
  )
  where
    parametersCount :: Natural
    parametersCount = fromIntegral (length parameters)

compileExpression ::
  F.Expression -> HashMap Name Integer -> [MF.Instruction]
compileExpression (F.Natural natural) _ =
  [Pushval $ Integer $ fromIntegral $ natural]
compileExpression (F.Application expression0 expression1) environment =
  compileExpression expression1 environment <>
  compileExpression expression0 (succ <$> environment) <>
  [Makeapp]
compileExpression (F.Variable name) environment
  | Just offset <- HM.lookup name environment = [Pushparam offset]
  | otherwise = [Pushfun name]
compileExpression (F.Let bindings expression) environment =
  fold
    (zipWith
      ($)
      (
        (toList $ fmap compileLocalExpression $ fmap snd $ bindings)
        <>
        [compileExpression expression]
      )
      (scanl
        (\environmentOld nameNew ->
          HM.insert nameNew (-1) (succ <$> environmentOld)
        )
        environment
        (toList $ fmap fst $ bindings)
      )
    )
  <>
  [SlideLet bindingsCount]
  where
    bindingsCount :: Natural
    bindingsCount = fromIntegral (length bindings)
    compileLocalExpression ::
      F.Expression -> HashMap Name Integer -> [Instruction]
    compileLocalExpression expression environment =
      compileExpression expression environment <> [Alloc, Makeapp]
