module F.Lex where

import F.Common (Name (MakeName))
import Data.Char
  ( digitToInt,
    isAlpha,
    isAlphaNum,
    isDigit,
    isSpace,
  )
import Data.Text qualified as T
import Relude

data Token =
  Seperator |
  Assign |
  Let | In |
  If | Then | Else |
  Or | And |
  Plus | Minus | Times | DividedBy |
  Equals | LessThan |
  Variable Name |
  Natural Natural |
  BracketOpen |
  BracketClose
  deriving (Show, Eq)

lex :: Text -> Either String [Token]
-- this justifies the use of unsafe T.head and T.tail below
lex input | T.null input = Right []
lex input
  | isSpace inputHead = lex inputTail
  | ';' <- inputHead = (Seperator :) <$> lex inputTail
  | '(' <- inputHead = (BracketOpen :) <$> lex inputTail
  | ')' <- inputHead = (BracketClose :) <$> lex inputTail
  | isAsciiSymbol inputHead =
     -- to-do. would `span isAsciiSymbol inputHead` be better?
    let (operator, rest) = T.span isAsciiSymbol input
    in
      (:)
        <$> (case operator of
          "=" -> Right Assign
          "|" -> Right Or
          "&" -> Right And
          "+" -> Right Plus
          "-" -> Right Minus
          "*" -> Right Times
          "/" -> Right DividedBy
          "==" -> Right Equals
          "<" -> Right LessThan
          _ ->
            Left ("operators cannot be user defined yet: " <> T.unpack input)
        )
        <*> lex rest
  | isAlpha inputHead =
    let
      (identifier, rest) = T.span (\c -> isAlphaNum c || c == '_') input
    in
      ((case identifier of
        "let" -> Let
        "in" -> In
        "if" -> If
        "then" -> Then
        "else" -> Else
        _ -> Variable (MakeName identifier)
      ) :)
      <$>
      lex rest
  | isDigit inputHead =
    let
      (numberText, rest) = T.span isDigit input
      number = T.foldl' step 0 numberText
      step :: Natural -> Char -> Natural
      step a c = a * 10 + fromIntegral (digitToInt c)
    in (Natural number :) <$> lex rest
  | otherwise = Left ("unexpected character: " <> T.unpack input)
  where
    inputHead = T.head input
    inputTail = T.tail input

isAsciiSymbol :: Char -> Bool
isAsciiSymbol c =
  c
  `elem`
  [
    '!', '#', '$', '%', '&', '*', '+', '.', '/', '<', '=', '>', '?',
    '@', '\\', '^', '|', '-', '~', ':'
  ]
