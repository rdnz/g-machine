module F.Parse where

import F.Common (Name (MakeName))
import F.F qualified as F
import F.Lex qualified as Lex
import Parse
  (
    TokenOrEndOfInput (Token),
    single,
    anySingle,
    fromAlternatives,
    runParser,
    option,
    many,
    some,
  )
import Parse qualified

import Relude hiding (many, some, optional)
import Relude.Extra.Foldable1

{-
Backus–Naur form

Program ::= Definition ; {Definition ;}
Definition ::= Variable {Variable} = Expression
Expression ::=
  let LocalDefinitions in Expression |
  if Expression then Expression else Expression |
  Expression1

Expression1 ::= Expression2 {| Expression2}
Expression2 ::= Expression3 {& Expression3}
Expression3 ::= Expression4 [ComparisonOperator Expression4]
Expression4 ::= Expression5 RestExpression4
RestExpression4 ::= {+ Expression5} | - Expression5
Expression5 ::= [-] Expression6
Expression6 ::= Expression7 RestExpression6
RestExpression6 ::= {* Expression7} | / Expression7
Expression7 ::= AtomicExpression {AtomicExpression}
AtomicExpression ::= Variable | Literal | ( Expression )

ComparisonOperator ::= == | <
LocalDefinitions ::= LocalDefinition {; LocalDefinition}
LocalDefinition ::= Variable = Expression

-}

type Parser = Parse.Parser Lex.Token

program :: Parser F.Program
program =
  some (definition <* single Lex.Seperator) isVariable

isVariable :: Lex.Token -> Bool
isVariable (Lex.Variable _) = True
isVariable _ = False

definition :: Parser F.TopLevelDefinition
definition =
  F.TopLevelDefinition
    <$> variable
    <*> many variable isVariable
    <* single Lex.Assign
    <*> expression

variable :: Parser Name
variable =
  do
    token <- anySingle
    case token of
      Lex.Variable name -> pure name
      _ -> fail "expected a variable"

expression :: Parser F.Expression
expression =
  fromAlternatives alternatives
  where
    application3 ::
      F.Expression ->
      F.Expression ->
      F.Expression ->
      F.Expression ->
      F.Expression
    application3 f a b c =
      f
        `F.Application` a
        `F.Application` b
        `F.Application` c
    alternatives (Token Lex.Let) =
      single Lex.Let *>
      (F.Let
        <$> localDefinitions
        <* single Lex.In
        <*> expression
      )
    alternatives (Token Lex.If) =
      single Lex.If *>
      (application3
        (fVariable "if")
        <$> expression
        <* single Lex.Then
        <*> expression
        <* single Lex.Else
        <*> expression
      )
    alternatives (Token token)
      | isExpression token = expression1
    alternatives _ = fail "expected an expression"

fVariable :: Text -> F.Expression
fVariable = F.Variable . MakeName

isExpression :: Lex.Token -> Bool
isExpression token =
  case token of
    Lex.Minus -> True
    Lex.Variable _ -> True
    Lex.Natural _ -> True
    Lex.BracketOpen -> True
    _ -> False

expression1 :: Parser F.Expression
expression1 =
  foldr1 (applyBinary "|") <$>
  ((:|)
    <$> expression2
    <*> many (single Lex.Or *> expression2) isOr
  )

applyBinary :: Text -> F.Expression -> F.Expression -> F.Expression
applyBinary f a b =
  fVariable f
    `F.Application` a
    `F.Application` b

isOr :: Lex.Token -> Bool
isOr Lex.Or = True
isOr _ = False

expression2 :: Parser F.Expression
expression2 =
  foldr1 (applyBinary "&") <$>
  ((:|)
    <$> expression3
    <*> many (single Lex.And *> expression2) isAnd
  )

isAnd :: Lex.Token -> Bool
isAnd Lex.And = True
isAnd _ = False

expression3 :: Parser F.Expression
expression3 =
  expression4
  <**>
  option
    id
    (applyBinaryFlipped <$> comparisonOperator <*> expression4)
    isComparisonOperator

applyBinaryFlipped ::
  Text -> F.Expression -> (F.Expression -> F.Expression)
applyBinaryFlipped f = flip (applyBinary f)

comparisonOperator :: Parser Text
comparisonOperator =
  do
    token <- anySingle
    case token of
      Lex.Equals -> pure "=="
      Lex.LessThan -> pure "<"
      _ -> fail "expected a comparison operator"

isComparisonOperator :: Lex.Token -> Bool
isComparisonOperator token =
  case token of
    Lex.Equals -> True
    Lex.LessThan -> True
    _ -> False

expression4 :: Parser F.Expression
expression4 = expression5

expression5 :: Parser F.Expression
expression5 =
  expression6 <**> restExpression5

restExpression5 :: Parser (F.Expression -> F.Expression)
restExpression5 =
  fromAlternatives alternatives
  where
    alternatives (Token Lex.Minus) =
      applyBinaryFlipped "-" <$> (single Lex.Minus *> expression6)
    alternatives _ =
      processAdditions <$> many (single Lex.Plus *> expression6) isPlus
    processAdditions :: [F.Expression] -> (F.Expression -> F.Expression)
    processAdditions [] = id
    processAdditions (operand0 : operandsRest) =
      applyBinaryFlipped "+" $
      foldr1 (applyBinary "+") $
      (operand0 :| operandsRest)

isPlus :: Lex.Token -> Bool
isPlus Lex.Plus = True
isPlus _ = False

expression6 :: Parser F.Expression
expression6 =
  option
    id
    (F.Application (fVariable "negate") <$ single Lex.Minus)
    isMinus
  <*>
  expression7

isMinus :: Lex.Token -> Bool
isMinus Lex.Minus = True
isMinus _ = False

expression7 :: Parser F.Expression
expression7 =
  expression8 <**> restExpression7

restExpression7 :: Parser (F.Expression -> F.Expression)
restExpression7 =
  fromAlternatives alternatives
  where
    alternatives (Token Lex.DividedBy) =
      applyBinaryFlipped "/" <$> (single Lex.DividedBy *> expression8)
    alternatives _ =
      processMultiplications
        <$> many (single Lex.Times *> expression8) isTimes
    processMultiplications ::
      [F.Expression] -> (F.Expression -> F.Expression)
    processMultiplications [] = id
    processMultiplications (operand0 : operandsRest) =
      applyBinaryFlipped "*" $
      foldr1 (applyBinary "*") $
      (operand0 :| operandsRest)

isTimes :: Lex.Token -> Bool
isTimes Lex.Times = True
isTimes _ = False

expression8 :: Parser F.Expression
expression8 =
  foldl1' F.Application
  <$>
  some atomicExpression isAtomicExpression

atomicExpression :: Parser F.Expression
atomicExpression =
  fromAlternatives alternatives
  where
    alternatives (Token (Lex.Variable _)) = F.Variable <$> variable
    alternatives (Token (Lex.Natural _)) = F.Natural <$> natural
    alternatives (Token Lex.BracketOpen) =
      single Lex.BracketOpen *> expression <* single Lex.BracketClose
    alternatives _ = fail "expected an atomic expression"

isAtomicExpression :: Lex.Token -> Bool
isAtomicExpression token =
  case token of
    Lex.Variable _ -> True
    Lex.Natural _ -> True
    Lex.BracketOpen -> True
    _ -> False

natural :: Parser Natural
natural =
  do
    token <- anySingle
    case token of
      Lex.Natural natural -> pure natural
      _ -> fail "expected a variable"

localDefinitions :: Parser (NonEmpty (Name, F.Expression))
localDefinitions =
  (:|)
    <$> localDefinition
    <*> many (single Lex.Seperator *> localDefinition) isSeperator

localDefinition :: Parser (Name, F.Expression)
localDefinition =
  (,)
    <$> variable
    <* single Lex.Assign
    <*> expression

isSeperator :: Lex.Token -> Bool
isSeperator Lex.Seperator = True
isSeperator _ = False

parse :: [Lex.Token] -> Either String F.Program
parse input =
  case runParser program input of
    (Left message, tokensRest) ->
      Left (message <> " instead of " <> show tokensRest)
    (result@(Right _), []) -> result
    (_, tokensRest) ->
      Left ("expected end of input instead of " <> show tokensRest)

data Foldr1Semigroup a = Foldr1Semigroup (a -> a -> a) a
unFoldr1Semigroup :: Foldr1Semigroup a -> a
unFoldr1Semigroup (Foldr1Semigroup _ a) = a

instance Semigroup (Foldr1Semigroup a) where
  Foldr1Semigroup _ a0 <> Foldr1Semigroup f a1 =
    Foldr1Semigroup f (f a0 a1)

foldr1 :: (Foldable1 f) => (a -> a -> a) -> f a -> a
foldr1 f = unFoldr1Semigroup . foldMap1 (Foldr1Semigroup f)
