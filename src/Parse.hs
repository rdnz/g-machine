module Parse where

import Relude hiding (many, some)

newtype Parser token a = Parser ([token] -> (Either String a, [token]))
runParser :: Parser token a -> [token] -> (Either String a, [token])
runParser (Parser function) = function

instance Functor (Parser token) where
  fmap f (Parser parser) =
    Parser
      (\tokensOld ->
        let (result, tokensNew) = parser tokensOld
        in (f <$> result, tokensNew)
      )

instance Applicative (Parser token) where
  pure result = Parser (\tokens -> (Right result, tokens))
  (Parser parser1) <*> (Parser parser2) =
    Parser
      (\tokensOld1 ->
        let
          (result1, tokensOld2) = parser1 tokensOld1
          (result2, tokensNew) = parser2 tokensOld2
        in case result1 of
          Left message -> (Left message, tokensOld2)
          Right f -> (f <$> result2, tokensNew)
      )

instance Monad (Parser token) where
  return = pure
  (Parser parser1) >>= f =
    Parser
      (\tokensOld1 ->
         let
           (result1, tokensOld2) = parser1 tokensOld1
         in case result1 of
           Left message -> (Left message, tokensOld2)
           Right parseResult ->
             let (Parser parser2) = f parseResult
             in parser2 tokensOld2
      )

instance MonadFail (Parser token) where
  fail message = Parser (\tokens -> (Left message, tokens))

single :: (Show token, Eq token) => token -> Parser token token
single token =
  Parser function
  where
    function tokensOld
      | tokenNext : tokensRest <- tokensOld, tokenNext == token =
        (Right tokenNext, tokensRest)
      | otherwise = (Left $ "expected " <> show token, tokensOld)

satisfy :: (token -> Bool) -> Parser token token
satisfy predicate =
  Parser function
  where
    function tokensOld
      | tokenNext : tokensRest <- tokensOld, predicate tokenNext =
        (Right tokenNext, tokensRest)
      | otherwise = (Left "expected something else", tokensOld)

anySingle :: Parser token token
anySingle = satisfy (const True)

data TokenOrEndOfInput token = Token token | EndOfInput

fromAlternatives ::
  (TokenOrEndOfInput token -> Parser token result) -> Parser token result
fromAlternatives alternatives =
  Parser
    (\tokens -> runParser (alternatives $ nextToken tokens) tokens)
  where
    nextToken [] = EndOfInput
    nextToken (token : _) = Token token

option :: r -> Parser token r -> (token -> Bool) -> Parser token r
option defaultResult parser parseAgain =
  fromAlternatives alternatives
  where
    alternatives (Token token)
      | parseAgain token = parser
    alternatives _ = pure defaultResult

many :: Parser token result -> (token -> Bool) -> Parser token [result]
many parser parseAgain =
  option [] ((:) <$> parser <*> many parser parseAgain) parseAgain

some :: Parser token a -> (token -> Bool) -> Parser token (NonEmpty a)
some parser parseAgain =
  (:|) <$> parser <*> many parser parseAgain
