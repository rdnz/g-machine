{-# OPTIONS_GHC
  -Wno-missing-exported-signatures
  -Wno-missing-signatures
  -Wno-unused-imports
  -Wno-incomplete-uni-patterns
#-}

module Example2 where

import F.Lex (lex)
import F.Parse (parse)
import F.Compile (compileProgram)
import F.Compile qualified as Compile
import F.MF (emulate, emulationResult, emulationStepCount)
import F.MF qualified as MF
import F (evaluationResult, evaluationStepCount)

import Text.Pretty.Simple (pPrint)
import Data.HashMap.Strict qualified as HM
import Relude
import Relude.Unsafe qualified as Unsafe

result =
  (=<<) compileProgram $
  (=<<) parse $
  lex $
  "main = not (6 == 2);"

Right (MF.Program instructions globalEnvironment) = result
main :: IO ()
-- main = print (emulationResult =<< result)
main =
  pPrint globalEnvironment *>
  pPrint instructions
  -- traverse (\(a, (_, b)) -> putStrLn (show a <> " " <> show b)) (sortOn (\(a , (_, (MF.MakeProgramCounter b))) -> b) (HM.toList globalEnvironment)) *>
  -- putStrLn "" *>
  -- traverse print instructions *>
  -- pure ()

-- > Example2.result
